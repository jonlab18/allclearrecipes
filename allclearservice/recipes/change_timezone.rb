script "change timezone" do
  interpreter "bash"
  user "root"
  code <<-EOH
	echo "Changing the timezone to EST"
	printf "ZONE=\"US/Eastern\"\nUTC=false\n" > /etc/sysconfig/clock
	ln -sf /usr/share/zoneinfo/US/Eastern /etc/localtime
	echo "Attemped to change the timezone"
  EOH
end